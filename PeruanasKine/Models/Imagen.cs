﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeruanasKine.Models
{
    public class Imagen
    {
        public int ImagenID { get; set; }
        public int ChicaID { get; set; }
        public bool perfil { get; set; }
        public string imagepath { get; set; }

        public virtual Chica Chica { get; set; }
    }
}