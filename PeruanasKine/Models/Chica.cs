﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeruanasKine.Models
{
    public class Chica
    {
        public Chica()
        {
            this.Imagens = new HashSet<Imagen>();
        }
       public int ChicaId { get; set; }
       public string Name { get; set; }
       public int edad { get; set; }
       
       public int ranking { get; set; }
       public string image { get; set; }
        public string titulo { get; set; }
        public string  descripcion { get; set; }
        public int precio { get; set; }
        public string telefono { get; set; }
        public string distrito { get; set; }

        public virtual ICollection<Imagen> Imagens { get; set; }
    }
}