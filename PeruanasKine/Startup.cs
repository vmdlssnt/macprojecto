﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PeruanasKine.Startup))]
namespace PeruanasKine
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
