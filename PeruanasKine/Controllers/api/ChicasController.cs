﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using PeruanasKine.Models;
using PeruanasKine.utilities;
using System.Drawing;


namespace PeruanasKine.Controllers.api
{
    public class ChicasController : ApiController
    {
        private ApplicationDbContext _context;
        public ChicasController()
        {
            _context = new ApplicationDbContext();
        }

        public IEnumerable<Chica> GetChicas()
        {
            return _context.Chica.ToList();

        }

        public Chica GetChica(int id)
        {
           Chica chica = _context.Chica.SingleOrDefault(c => c.ChicaId == id);
            
            if(chica == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return chica;

        }
        // localhost:56514/api/Chicas/9
        [Authorize]
        [HttpPost]
        public void DeleteChica(int id)
        {
            Chica chica = GetChica(id);
            if (chica == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            _context.Chica.Remove(chica);
            _context.SaveChanges();
        }

        [Authorize]
        [HttpPost]
        public void SetProfilePic(int id, [FromBody]string source)
        {
            var imagesforthischica = _context.Imagen.Where(c => c.ChicaID == id).ToList();
            imagesforthischica.ForEach(c => c.perfil = false);
            var newprofilepic = _context.Imagen.SingleOrDefault(c => c.imagepath == source);
            newprofilepic.perfil = true;
            var chica = _context.Chica.SingleOrDefault(c => c.ChicaId == id);
            chica.image = source;
            _context.SaveChanges();


        }
        [Authorize]
        [HttpPost]
        public void DeleteImagesFromChica(int id,[FromBody]Unwantedimages unwantedimages)
        {
            
            var receivedunwantedimages = unwantedimages.unwantedimages;
            var testimage = receivedunwantedimages[0].src;
            foreach(var element in receivedunwantedimages)
            {
                _context.Imagen.RemoveRange(_context.Imagen.Where(c => c.imagepath == element.src));
                _context.SaveChanges();
                    //db.ProRel.RemoveRange(db.ProRel.Where(c => c.ProjectId == Project_id));
            }
            var otherimages = _context.Imagen.Where(c => c.ChicaID ==id);
            var chica = _context.Chica.SingleOrDefault(c => c.ChicaId == id);

            //if there are other images and chica.imagepath is not included in image table
            //then assign chica.imagepath to first encounter in image table
            if(otherimages.Count()>0 && otherimages.Where(c => c.imagepath == chica.image).Count() <= 0)
            {
                var firstimage = _context.Imagen.First(c => c.ChicaID == id);
                firstimage.perfil = true;
                chica.image = firstimage.imagepath;

                _context.SaveChanges();

            }




            var request = HttpContext.Current.Request;
            
            Debug.WriteLine("trolismo");

        }
        [Authorize]
        [HttpPost]
        public void AddImage(int id)
        {
            try { 
            var myfile = HttpContext.Current.Request.Files[0].InputStream;
            var x = Convert.ToInt32(double.Parse(HttpContext.Current.Request.Form[0]));
            var y = Convert.ToInt32(double.Parse(HttpContext.Current.Request.Form[1]));
            var w = Convert.ToInt32(double.Parse(HttpContext.Current.Request.Form[2]));
            var h = Convert.ToInt32(double.Parse(HttpContext.Current.Request.Form[3]));

            //System.Drawing.Image OriginalImage = System.Drawing.Image.FromFile(myfile.FileName);
            System.Drawing.Image OriginalImage = System.Drawing.Image.FromStream(myfile);
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(w, h);
            System.Drawing.Graphics Graphic = System.Drawing.Graphics.FromImage(bmp);
            Graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            Graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            Graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            Graphic.DrawImage(OriginalImage, new System.Drawing.Rectangle(0, 0, w, h), x, y, w, h, GraphicsUnit.Pixel);
            MemoryStream ms = new MemoryStream();
            bmp.Save(ms, OriginalImage.RawFormat);

            var mybuffer = ms.GetBuffer();

            byte[] CropImage = mybuffer;
            ms.Write(CropImage, 0, CropImage.Length);


            var path = Path.Combine(
                    HttpContext.Current.Server.MapPath("../../Content/images/"),
                    Path.GetFileName(HttpContext.Current.Request.Files[0].FileName));

            System.Drawing.Image CroppedImage = System.Drawing.Image.FromStream(ms, true);
            CroppedImage.Save(path,CroppedImage.RawFormat);



                var chica = _context.Chica.SingleOrDefault(c=>c.ChicaId==id);
                var newimage = new Imagen();
                newimage.ChicaID = id;
                newimage.perfil = false;
                newimage.Chica = chica;
                newimage.imagepath = "../../Content/images/" + HttpContext.Current.Request.Files[0].FileName;
                _context.Imagen.Add(newimage);
                _context.SaveChanges();


            }catch (Exception e)
            {

            }





        }

        //localhost:56514/api/Chicas/EditChica?id=16
        [Authorize]
        [HttpPost]
        public void EditChica(int id)
        {
            

            var chica = _context.Chica.SingleOrDefault(c => c.ChicaId == id); 
            if(chica == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            var myform = HttpContext.Current.Request.Form;
            var myfile = HttpContext.Current.Request.Files[0];
            var fileName = Path.GetFileName(myfile.FileName);

            chica.Name = myform[0];
            chica.edad = Int32.Parse(myform[1]);
            //newchica.opinion = myform[2];
            chica.ranking = Int32.Parse(myform[2]);
            chica.precio = Int32.Parse(myform[3]);
            chica.titulo = myform[4];
            chica.descripcion = myform[5];
            chica.distrito = myform[6];
//            chica.image = ("../../Content/images/" + myfile.FileName);

            
            _context.SaveChanges();


            //TO BE USED WHILE DOING LOCAL TESTING
            int i = 0;
            foreach (var element in HttpContext.Current.Request.Files)
            {
                if (myfile.FileName == "")
                {
                    break;
                }
                var newimage = new Imagen();
                newimage.Chica = chica;
                newimage.ChicaID = chica.ChicaId;


                var otherimages = _context.Imagen.Where(c => c.ChicaID == chica.ChicaId).Where(c=>c.perfil==true);
                if (i == 0 && otherimages.Count()<1)
                {
                    chica.image = "../../Content/images/" + HttpContext.Current.Request.Files[i].FileName;
                    newimage.perfil = true;
                    newimage.imagepath = "../../Content/images/" + HttpContext.Current.Request.Files[i].FileName;

                }
                else
                {
                    newimage.perfil = false;
                    newimage.imagepath = "../../Content/images/" + HttpContext.Current.Request.Files[i].FileName;


                }
                var path = Path.Combine(
                    HttpContext.Current.Server.MapPath("../../Content/images/"),
                    Path.GetFileName(HttpContext.Current.Request.Files[i].FileName));
                HttpContext.Current.Request.Files[i].SaveAs(path);
                Debug.WriteLine(i + "+" + path);
                i++;
                _context.Imagen.Add(newimage);
                _context.SaveChanges();
            }



            /*
            chica.Name = newchicainfo.Name;
            chica.edad = newchicainfo.edad;
            chica.ranking = newchicainfo.ranking;
            chica.image = newchicainfo.image;
            chica.titulo = newchicainfo.titulo;
            chica.descripcion = newchicainfo.descripcion;
            chica.precio = newchicainfo.precio;
            chica.telefono = newchicainfo.telefono;
            chica.distrito = newchicainfo.distrito;
            */
            //_context.SaveChanges();

        }
        [Authorize]
        [HttpPost]
        public String CreateChica()
        {
            var myform = HttpContext.Current.Request.Form;


            
            var x = Convert.ToInt32(double.Parse(HttpContext.Current.Request.Form["x"]));
            var y = Convert.ToInt32(double.Parse(HttpContext.Current.Request.Form["y"]));
            var w = Convert.ToInt32(double.Parse(HttpContext.Current.Request.Form["w"]));
            var h = Convert.ToInt32(double.Parse(HttpContext.Current.Request.Form["h"]));


            Debug.WriteLine(HttpContext.Current.Request.Files.Count);
            
            var myfile = HttpContext.Current.Request.Files[0];
            

            var fileName = Path.GetFileName(myfile.FileName);
            var newchica = new Chica();


            //TO BE USED ON DEPLOYMENT ENVIRONMENT

            /*
            FtpWebRequest request = (FtpWebRequest) WebRequest.Create("ftp://50.62.160.73/" + "httpdocs/Content/images/" + fileName);
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential("rdelosh","Carlosex178!");

            Stream fs = myfile.InputStream;
            byte[] buffer = new byte[fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            fs.Close();
            Stream ftpStream = request.GetRequestStream();
            ftpStream.Write(buffer,0,buffer.Length);
            ftpStream.Close();
            */
            //end of deployment environment section


            newchica.Name = myform[0];
            newchica.edad = Int32.Parse(myform[1]);
            //newchica.opinion = myform[2];
            newchica.ranking = Int32.Parse(myform[2]);
            newchica.precio = Int32.Parse(myform[3]);
            newchica.titulo = myform[4];
            newchica.descripcion = myform[5];
            newchica.distrito = myform[6];

            newchica.image = ("../../Content/images/" + myfile.FileName);

            _context.Chica.Add(newchica);
            _context.SaveChanges();
            //TO BE USED WHILE DOING LOCAL TESTING


            int i = 0;
            foreach (var element in HttpContext.Current.Request.Files)
            {
                var newimage = new Imagen();
                newimage.Chica = newchica;
                newimage.ChicaID = newchica.ChicaId;

                
                
                if (i == 0)
                {
                    newchica.image = "../../Content/images/" + HttpContext.Current.Request.Files[i].FileName;
                    newimage.perfil = true;
                    newimage.imagepath = "../../Content/images/" + HttpContext.Current.Request.Files[i].FileName;

                }
                else
                {
                    newimage.perfil = false;
                    newimage.imagepath = "../../Content/images/" + HttpContext.Current.Request.Files[i].FileName;


                }
                System.Drawing.Image OriginalImage = System.Drawing.Image.FromStream(HttpContext.Current.Request.Files[0].InputStream);
                System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(w, h);
                System.Drawing.Graphics Graphic = System.Drawing.Graphics.FromImage(bmp);
                Graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                Graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                Graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                Graphic.DrawImage(OriginalImage, new System.Drawing.Rectangle(0, 0, w, h), x, y, w, h, GraphicsUnit.Pixel);
                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, OriginalImage.RawFormat);

                var mybuffer = ms.GetBuffer();

                byte[] CropImage = mybuffer;
                ms.Write(CropImage, 0, CropImage.Length);


                var path = Path.Combine(
                        HttpContext.Current.Server.MapPath("../../Content/images/"),
                        Path.GetFileName(HttpContext.Current.Request.Files[0].FileName));

                System.Drawing.Image CroppedImage = System.Drawing.Image.FromStream(ms, true);
                CroppedImage.Save(path, CroppedImage.RawFormat);



                i++;
                _context.Imagen.Add(newimage);
                _context.SaveChanges();
            }

            Debug.WriteLine("lalismo"+newchica.Imagens);



            //end of local testing section







            return null;
        }
        
        




    }
}
