namespace PeruanasKine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_distrito_to_chica : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Chicas", "distrito", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Chicas", "distrito");
        }
    }
}
