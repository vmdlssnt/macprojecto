namespace PeruanasKine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedIDtochicaID : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Chicas");
            DropColumn("dbo.Chicas", "Id");
            AddColumn("dbo.Chicas", "ChicaId", c => c.Int(nullable: false, identity: true));
            
            AddPrimaryKey("dbo.Chicas", "ChicaId");
            
        }
        
        public override void Down()
        {

            DropPrimaryKey("dbo.Chicas");
            DropColumn("dbo.Chicas", "Id");
            AddColumn("dbo.Chicas", "ChicaId", c => c.Int(nullable: false, identity: true));

            AddPrimaryKey("dbo.Chicas", "ChicaId");


            /*
            AddColumn("dbo.Chicas", "Id", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Chicas");
            DropColumn("dbo.Chicas", "ChicaId");
            AddPrimaryKey("dbo.Chicas", "Id");
            */
        }
    }
}
