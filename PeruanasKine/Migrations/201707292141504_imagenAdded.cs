namespace PeruanasKine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class imagenAdded : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Chicas");
            CreateTable(
                "dbo.Imagens",
                c => new
                    {
                        ImagenID = c.Int(nullable: false, identity: true),
                        ChicaID = c.Int(nullable: false),
                        perfil = c.Boolean(nullable: false),
                        imagepath = c.String(),
                    })
                .PrimaryKey(t => t.ImagenID)
                .ForeignKey("dbo.Chicas", t => t.ChicaID, cascadeDelete: true)
                .Index(t => t.ChicaID);
            DropColumn("dbo.Chicas", "Id");
            AddColumn("dbo.Chicas", "ChicaId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Chicas", "ChicaId");
            
        }
        
        public override void Down()
        {
            AddColumn("dbo.Chicas", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.Imagens", "ChicaID", "dbo.Chicas");
            DropIndex("dbo.Imagens", new[] { "ChicaID" });
            DropPrimaryKey("dbo.Chicas");
            DropColumn("dbo.Chicas", "ChicaId");
            DropTable("dbo.Imagens");
            AddPrimaryKey("dbo.Chicas", "Id");
        }
    }
}
