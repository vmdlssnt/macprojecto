namespace PeruanasKine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed_image_datatype_inChica : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Chicas", "image", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Chicas", "image", c => c.Binary());
        }
    }
}
