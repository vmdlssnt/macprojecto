namespace PeruanasKine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_props_to_chica : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Chicas", "titulo", c => c.String());
            AddColumn("dbo.Chicas", "descripcion", c => c.String());
            AddColumn("dbo.Chicas", "precio", c => c.Int(nullable: false));
            AddColumn("dbo.Chicas", "telefono", c => c.String());
            DropColumn("dbo.Chicas", "opinion");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Chicas", "opinion", c => c.String());
            DropColumn("dbo.Chicas", "telefono");
            DropColumn("dbo.Chicas", "precio");
            DropColumn("dbo.Chicas", "descripcion");
            DropColumn("dbo.Chicas", "titulo");
        }
    }
}
