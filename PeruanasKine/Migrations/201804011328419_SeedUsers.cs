namespace PeruanasKine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'49d1a9f3-903a-4c95-a07a-37fc6e9ec57c', N'macadmin@perukine.com', 0, N'AN/nUdHgHuFdmqCV6FdZj19hyhSc5mwyy+4slv8pXG7ug8+8eRPsUKi3AHt3yuK9Bw==', N'9cd32022-bcf0-4490-aa60-71d25456bcf5', NULL, 0, 0, NULL, 1, 0, N'macadmin@perukine.com')

            INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'6cee8f20-fac2-43a4-974f-5883e74051a1', N'Admin')

            INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'49d1a9f3-903a-4c95-a07a-37fc6e9ec57c', N'6cee8f20-fac2-43a4-974f-5883e74051a1')


            ");
        }
        
        public override void Down()
        {
        }
    }
}
