namespace PeruanasKine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_virtualprops : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Imagens",
                c => new
                    {
                        ImagenID = c.Int(nullable: false, identity: true),
                        ChicaID = c.Int(nullable: false),
                        perfil = c.Boolean(nullable: false),
                        imagename = c.String(),
                    })
                .PrimaryKey(t => t.ImagenID)
                .ForeignKey("dbo.Chicas", t => t.ChicaID, cascadeDelete: true)
                .Index(t => t.ChicaID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Imagens", "ChicaID", "dbo.Chicas");
            DropIndex("dbo.Imagens", new[] { "ChicaID" });
            DropTable("dbo.Imagens");
        }
    }
}
