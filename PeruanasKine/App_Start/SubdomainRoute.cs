﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PeruanasKine.App_Start
{
    public class SubdomainRoute : RouteBase
    {
        
        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            if (httpContext.Request == null || httpContext.Request.Url == null)
            {
                return null;
            }

            var host = httpContext.Request.Url.Host;
            var index = host.IndexOf(".");
            string[] segments = httpContext.Request.Url.PathAndQuery.TrimStart('/').Split('/');

            if (index < 0)
            {
                return null;
            }
            var routeData = new RouteData(this, new MvcRouteHandler());




            var subdomain = host.Substring(0, index);
            if (subdomain.Equals("admin"))
            {
                routeData.Values.Add("controller", "Admin"); //Goes to the relevant Controller  class
                routeData.Values.Add("action", "AdminPortal");
                
                
                return routeData;
            }
            string[] blacklist = { "www", "localhost:56514","peruanaskine.com:56514", "mail" };

            if (blacklist.Contains(subdomain))
            {
                return null;
            }

            string controller = (segments.Length > 1) ? segments[0] : "Home";
            string action = (segments.Length > 1) ? segments[1] : "Index";
            //System.Diagnostics.Debug.WriteLine("LALALALLALALA" + controller);
            //System.Diagnostics.Debug.WriteLine(segments.Length);

            
            routeData.Values.Add("controller", controller); //Goes to the relevant Controller  class
            routeData.Values.Add("action", action); //Goes to the relevant action method on the specified Controller
            routeData.Values.Add("subdomain", subdomain); //pass subdomain as argument to action method
            return routeData;
        }

        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            //Implement your formating Url formating here
            return null;
        }
        
    }
    
}
