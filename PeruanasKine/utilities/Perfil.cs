﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PeruanasKine.Models;

namespace PeruanasKine.utilities
{
    public class Perfil
    {
        public Chica chica { get; set; }
        public HttpPostedFileBase file{ get; set; }
    }
}